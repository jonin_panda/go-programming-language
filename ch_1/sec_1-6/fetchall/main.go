package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func main() {
	start := time.Now()

	ch := make(chan string) //making a bidirectional channel that sends or receives strings
	for _, url := range os.Args[1:] {
		go fetch(url, ch) //starting a go routine calling the fetch function
	}
	for range os.Args[1:] {
		fmt.Println(<-ch)
	}
	fmt.Printf("%.2fs elapsed\n", time.Since(start).Seconds()) //last line of code written to output before exiting the program
	f, err := os.OpenFile("url.txt", os.O_RDWR | os.O_APPEND, 0644)
	if err != nil {
		fmt.Printf("Error while writing to file %s", err)
	}
	_, err = fmt.Fprintln(f, fmt.Sprintf("%.2fs elapsed\n", time.Since(start).Seconds()))
	if err != nil {
		fmt.Printf("Error while writing to file %s", err)
	}
	f.Close()
}

func fetch(url string, ch chan<- string) {
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprint(err) // send to channel ch
		return
	}
	f, err := os.OpenFile("url.txt", os.O_CREATE | os.O_RDWR | os.O_APPEND, 0644)
	if err != nil {
		fmt.Printf("Error while creating to file %s", err)
	}
	nbytes, err := io.Copy(ioutil.Discard, resp.Body) //ioutil reads the body of the response and Discards it
	resp.Body.Close() //don't leak resources
	if err != nil {
		ch <- fmt.Sprintf("while reading %s: %v", url, err)
		return
	}
	secs := time.Since(start).Seconds()
	_, err = fmt.Fprintln(f, fmt.Sprintf("%.2fs %7d %s", secs, nbytes, url))
	if err != nil {
		fmt.Printf("Error while writing to file %s", err)
	}
	ch <- fmt.Sprintf("%.2fs %7d %s", secs, nbytes, url)
	f.Close()
}
