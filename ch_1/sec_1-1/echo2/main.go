package main

import (
	"fmt"
	"os"
)

func main() {
	s, sep := "", ""

	/*
	another loop uses the range keyword to access each element in an array
	*/
	for _, arg := range os.Args[1:] {
		s += sep + arg
		sep = " "
	}
	fmt.Println(s)
}
