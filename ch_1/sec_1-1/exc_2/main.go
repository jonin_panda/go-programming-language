package main

import (
	"fmt"
	"os"
)

func main() {
	//here we are using a range loop to access each element in the os.Args slice
	//however instead of throwing the index value away wer are including it in the output
	for i, v := range os.Args[1:] {
		fmt.Println(i, v)
	}
}