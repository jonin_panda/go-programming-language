package main

import (
	"fmt"
	"os"
)

func main() {
	var s, sep string

	//Like echo1 we are using a for loop to access each element in the slice os.Args
	//in this example though, we are setting the initialization to 0
	//bc the first element in the slice os.Args is the command it self
	for i := 0; i < len(os.Args); i++ {
		s += sep + os.Args[i]
		sep = " "
	}
	fmt.Println(s)
}
