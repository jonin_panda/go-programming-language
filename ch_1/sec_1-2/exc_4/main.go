package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int) //creating a new map with keys of string type and values of int type

	fileName := make(map[string]string) //***NEW: creating another map to map the content of the file to the file name

	files := os.Args[1:]           //this line is holding a slice of type os.Args

	//we first check to see if any files have been input as arguments with the command
	//if no files are provided then we call count lines and read from Stdin
	//else we will range over the list of files, open the files, check for errors, then call countLines
	if len(files) == 0 {
		countLines(os.Stdin, counts, fileName)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
				continue
			}
			countLines(f, counts, fileName)
			f.Close() //here we are closing the files
		}
	}

	//this ranges over the counts map and if multiple lines are > 1 it will print the line
	//and will print the associated content to the line
	for line, n := range counts {
		if n > 1 {
			fmt.Println(line, fileName[line])
		}
	}
}




//Function countLines reads input from Stdin
func countLines(f *os.File, counts map[string]int, fileName map[string]string) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
		fileName[input.Text()] = f.Name()
	}
}

