package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	counts := make(map[string]int)

	//this loop is ranging over the files provided by Stdin
	//it then uses ioutil to open and read the file and stores
	for _, filename := range os.Args[1:] {
		data, err := ioutil.ReadFile(filename) //ReadFile reads in the contents of the file
		if err != nil {
			fmt.Fprintf(os.Stderr, "dup3: %v\n", err)
			continue
		}
		//Split from the strings package will take the contents in data and split all substrings by the \n sep
		//this range loop adds each string sep by \n into the map counts
		for _, line := range strings.Split(string(data), "\n") {
			counts[line]++
		}
	}
	//this ranges over the map counts and if any lines are > 1 it will print the duplicates
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}
}
