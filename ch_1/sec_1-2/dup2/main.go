package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int) //creating a new map with keys of string type and values of int type
	files  := os.Args[1:] //this line is holding a slice of type os.Args

	//we first check to see if any files have been input as arguments with the command
	//if no files are provided then we call count lines and read from Stdin
	//else we will range over the list of files, open the files, check for errors, then call countLines
	if len(files) == 0 {
		countLines(os.Stdin, counts)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Println(os.Stderr, "dup2: %v\n", err)
				continue
			}
			countLines(f, counts)
			f.Close() //here we are closing the files
		}
	}
	//This for loop will only print out if input from Stdin is greater than 1
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}
}

//Function countLines reads input from Stdin
func countLines(f *os.File, counts map[string]int) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
	}
}
