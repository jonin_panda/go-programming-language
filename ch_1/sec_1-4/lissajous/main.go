//Lissajous generates GIF animations of random Lissajous figures.
package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
)

var palette = []color.Color{color.Black, color.RGBA{0, 255, 0, 255}, color.RGBA{0,0,255,255}, color.RGBA{255,0,255, 255}, color.RGBA{0,255,255,255}}

const (
	whiteIndex = 0 //first color in palette
)


func main() {
	lissajous(os.Stdout)
}

func lissajous(out io.Writer) {
	const (
		cycles  = 5     //number of complete x oscillator revolutions
		res     = 0.001 //angular resolution
		size    = 100   //image canvas covers [-size..+size]
		nframes = 64    //number of animation frames
		delay   = 8     //delay between frames in 10ms units
	)



	freq := rand.Float64() * 3.0 //relative frequency of y oscillator
	anim := gif.GIF{LoopCount: nframes} //this is a struct type
	phase := 0.0 //phase difference

	//This outer loop
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img  := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			blackIndex := rand.Intn(5)
			img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5), uint8(blackIndex))
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay) //appending the Delay value to anim; Delay is a field within the struct gif.Gif
		anim.Image = append(anim.Image, img) //appending the Image value to anim; Image is a field within the struct gif.Gif
	}
	gif.EncodeAll(out, &anim) //NOTE: ignoring encoding errors
}
