//Package 2 Program Structure
//-------------------------------
//The names of Go functions, variables, constants, types, statement labels, and packagesfollow a simple rule: a name begins with a letter (that is, anything thatUnicode deems a letter) or an underscore and may have any number ofadditional letters, digits, and underscores which are all case sensitive
//If an entity is declared within a function, it is local to that function
//If the namebegins with an upper-case letter, it isexported, which means that it is visible and accessible outsideof its own package and may be referred to by other parts of the program
// If the name begins with an upper-case letter, it is exported, which means that it is visible and accessible outside of its own package and may be referred to by other parts of the program
//There are four major kinds of declarations: var,const, type, and func
//A Go program is stored in one or more files whose names end in.go.  Each file begins with a package declaration that sayswhat package the file is part of.The package declaration is followed byany import declarations, and then a sequence of package-leveldeclarations of types, variables, constants, and functions, in any order

package ch_2
