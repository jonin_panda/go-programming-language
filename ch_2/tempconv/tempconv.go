package tempconv

import "fmt"

type Celsius float64 //declaring a new type, since the type starts with a capital letter the type can be seen outside the package
type Fahrenheit float64

const (
	AbsoluteZeroC Celsius = -273.15
	FreezingC     Celsius = 0
	BoilingC	  Celsius = 100
)

func (c Celsius) String() string { return fmt.Sprintf("%g%cC", c, 176)}
func (f Fahrenheit) String() string { return fmt.Sprintf("%g%cF", f, 176)}


